package bitbucket.ianai.hyperreal;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.lang.ArithmeticException;


public class HyperReal {
	public static final int MAX_DIGITS = 30; //all HyperReals will be represented to this many places
	public List<BigDecimal> digits = new ArrayList<BigDecimal>();//the array storing the digits of the HyperReal
	public boolean standardPartExists;
	public BigDecimal standardPart;
	public BigDecimal minDigit;
	public BigDecimal maxDigit;
	public boolean isDec;
	public boolean isInc;	

	
	public HyperReal(){
		//The simplest HyperReal constructor initializes the digits array to all 0s
		for(int digit=0; digit<MAX_DIGITS; digit++) {
			this.digits.add(digit, new BigDecimal(0));
		}
		this.isDec = true; //initialize these here since we're not calling the func_standardPartExists
		this.isInc = true;
		this.minDigit = new BigDecimal(0);
		this.maxDigit = new BigDecimal(0);
		this.standardPartExists = true;
		this.standardPart = new BigDecimal(0);

		}
	
	public HyperReal(double init_val) {
		//initializes the HyperReal from a single, double
		//this is the basis for converting "real numbers" (i.e. ints, doubles, and other real number analogs) to a hyperreal directly.
		
		for(int place=0; place<MAX_DIGITS; place++) {
			this.digits.add(place, new BigDecimal(init_val) );
		}
		this.isDec = true;
		this.isInc = true;	
		this.maxDigit = new BigDecimal(init_val);
		this.minDigit = new BigDecimal(init_val);
		this.standardPartExists = true;
		this.standardPart = new BigDecimal(init_val);

	};
	
	public HyperReal(double[] init_list) {
		//initializes the HyperReal from an array of doubles
		//the length of init_list should be HyperReal.MAX_DIGITS
		if(init_list.length >= MAX_DIGITS) {
			for(int digit=0; digit<HyperReal.MAX_DIGITS; digit++) {
				this.digits.add(new BigDecimal(init_list[digit]));
			}
			this.standardPartExists = func_standardPartExists();
			this.minDigit = Collections.min(this.digits);
			this.maxDigit = Collections.max(this.digits);
			this.standardPart = func_standardPart();

		}
		};
	
	public HyperReal(List<BigDecimal> init_list) {
		if( init_list.size() == HyperReal.MAX_DIGITS) {
			this.digits.addAll(init_list);
			this.standardPartExists = func_standardPartExists();
			this.minDigit = Collections.min(this.digits);
			this.maxDigit = Collections.max(this.digits);
			this.standardPart = func_standardPart();
		} else {
			System.out.println("HyperReal(List<BigDecimal>) expects a list with size " +  HyperReal.MAX_DIGITS );
		}
	}
	
	public int compareTo(HyperReal other) {
		//first compares standard parts, if they exist
		//then compares max and min digits, depending on whether the hyperreal sequences are increasing or decreasing
		//then compares both max and min at the same time. If the max and min of a sequence are always greater than the other, then
		//that number is considered greater than the other (and similar for less than).
		//the case where none of the above stands is set as "equal" - this is probably an error or will be resolved with a better implementation.
		int value = 0;
		if(this.standardPartExists && other.standardPartExists) {
			value = this.standardPart.compareTo(other.standardPart);
		}
		else {
			if(this.isDec && other.isDec) {
				value = (this.minDigit.compareTo(other.minDigit));
			}
			else if (this.isInc && other.isInc) {
				value = (this.maxDigit.compareTo(other.maxDigit));
			}
			else if (this.isInc && other.isDec) {
				value = (this.maxDigit.compareTo(other.minDigit));
			}
			else if (this.isDec && other.isInc) {
				value = this.minDigit.compareTo(other.maxDigit);
			} 
			else {
				//one or both are not increasing and not decreasing
				if ( (this.maxDigit.compareTo(other.maxDigit)) == (this.minDigit.compareTo(other.minDigit)) ) {
					value = this.maxDigit.compareTo(other.maxDigit) ;
				}
				else {
					//neither dominates the other. 
					//I'll leave it as they're equal for now.
					value = 0;
				}
			}
		}
		return(value);
	}
	
	public boolean func_isDecreasing() {
		//tests for monotonic decreasing
		boolean value = true;
		int compare;
		for( int place=0; place<MAX_DIGITS-1; place++) {
			compare = this.digits.get(place).compareTo(this.digits.get(place+1));
			if( compare < 0 ){
				value=false;
			}
		}
		return(value);
		
	}
	
	public boolean func_isIncreasing() {
		//tests for monotonic increasing
		boolean value = true;
		int compare;
		for( int place=0; place<MAX_DIGITS-1; place++) {
			compare = this.digits.get(place).compareTo(this.digits.get(place+1));
			if( compare > 0 ){
				value=false;
			}
		}
		return(value);
	}
		
	public boolean func_standardPartExists() {
		//currently just tests if the sequence increases or decreases.
		//an actual limit testing algorithm is needed at some point.
		boolean value=false;
	
		this.isDec = this.func_isDecreasing(); //saving these tests at the instance level
		this.isInc = this.func_isIncreasing(); //they come in handy elsewhere
		
		if( isDec || isInc) {
			value = true;
		}
		return(value);
	}
	public BigDecimal func_standardPart() {
		//returns minDigit if it's decreasing, and maxDigit if it's increasing
		//a real limit algorithm is needed.
		//this.minDigit and this.maxDigit must be already calculated before calling this function.
		if (this.standardPartExists) {
			if(this.isDec) {
				return(this.minDigit);
			}
			if(this.isInc) {
				return(this.maxDigit);		
			}
		} else {
			System.out.println("standpardPart doesn't exist");
		}
		return(new BigDecimal(0));
	}
	
	public String toString() {
		//returns the HyperReal's digit

		return(this.digits.toString() );
	}
	
	public static HyperReal add(HyperReal a, HyperReal b) {
		//component aka digit-wise addition
		List<BigDecimal> newDigits = new ArrayList<BigDecimal>();
		
		for(int place=0; place < HyperReal.MAX_DIGITS; place++) {
			newDigits.add( a.digits.get(place).add( b.digits.get(place) ) );
		}
		
		return(new HyperReal(newDigits));
		
	}
	
	public static HyperReal multiply(HyperReal a, HyperReal b) {
		//component wise multiplication
		List<BigDecimal> newDigits = new ArrayList<BigDecimal>();
		
		for(int place=0; place<HyperReal.MAX_DIGITS; place++) {
			newDigits.add( a.digits.get(place).multiply(b.digits.get(place)));
		}
		return(new HyperReal(newDigits));
	}
	
	public static HyperReal divide(HyperReal a, HyperReal b) throws ArithmeticException {
		//component wise division
		//will only carry-out division where b.digits is not zero.
		//throws an exception if any of the divisor's digits are zero.
		List<BigDecimal> newDigits = new ArrayList<BigDecimal>();
		BigDecimal zero = new BigDecimal(0);
		for(int place=0; place<HyperReal.MAX_DIGITS; place++){
			
			if(b.digits.get(place) != zero) {
				newDigits.add( a.digits.get(place).divide(b.digits.get(place)) );
			} else {
				//division by zero...
				throw new ArithmeticException("One or more digits in the sequence of the divisor HyperReal were zero.");
			}
				
		}
		return(new HyperReal(newDigits));
	}

}
