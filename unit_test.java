/**
 * 
 */
package bitbucket.ianai.hyperreal;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author work
 *
 */
public class unit_test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HyperReal a = new HyperReal();
		HyperReal b = new HyperReal(40);
		HyperReal c = new HyperReal(30.8);
		HyperReal one = new HyperReal(1);
		HyperReal negOne = new HyperReal(-1);
		HyperReal op1 = HyperReal.add(a, b);
		HyperReal op2 = HyperReal.multiply(b,  c);
		
		List<BigDecimal> newDigits = new ArrayList<BigDecimal>();
		BigDecimal number = new BigDecimal(0.5);
		
		for(int digit = 0; digit < HyperReal.MAX_DIGITS; digit++) {
			newDigits.add(digit, number.pow(digit) );
		}
		HyperReal infinitesimal = new HyperReal( newDigits);
		
		System.out.println("op1 = " + op1 + " op1.standardPart = " + op1.standardPart );
		System.out.println("op2 standard part = " + op2.standardPart);
		//this next part shows how the standardPart of infinitesimal will approach the true standardPart
		System.out.println("infinitesimal = " + infinitesimal + " with standardPart = " + infinitesimal.standardPart + " isDec = " + infinitesimal.isDec + " isInc = " + infinitesimal.isInc);
		System.out.println("Compare infinitesimal to HyperReal(40) = " + infinitesimal.compareTo(b) );
		System.out.println("Compare infinitesimal to HyperReal(1) = " + infinitesimal.compareTo(one) );
		System.out.println("Compare infinitesimal to HyperReal(-1) = " + infinitesimal.compareTo(negOne) );
	}

}
